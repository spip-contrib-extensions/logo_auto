# Logos automatiques

![logo_auto](logo_auto-xx.svg)

Cherche le logo d'un contenu précis

Dans l'ordre, si aucun logo n'a été trouvé avant :
- image jointe au contenu, meilleure résolution
- vignette de document joint au contenu, meilleure résolution
